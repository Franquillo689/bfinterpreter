#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define MEMORY 30000
#define ERROR  7

FILE *fp;
// *fp points to cursor on program in bf
// *fpBegin points to start of program (.bf)

int mem[MEMORY];
int *cursor;
int *cursorBegin;

/*
 * Brainfuck statements:
 * + Add 1
 * - Substract 1
 * ] Loop start
 * [ Loop end
 * . Print
 * , Input
 * < Move cursor to left
 * > Move cursor to right
 */

char getCurrentChar(){
	int c = 0;
	if(!feof(fp))
		c = fgetc(fp);
	switch(c){
	case 43: return '+';
	case 45: return '-';
	case 91: return '[';
	case 93: return ']';
	case 46: return '.';
	case 44: return ',';
	case 60: return '<';
	case 62: return '>';
	default: return '@';
	}
}

void moveRight(){
	if (cursor != &mem[-1+sizeof(mem)/sizeof((int)1)])
		cursor++;
	else { 
		printf("Error at memory pointer!");
		exit(ERROR);
	}
}

void moveLeft(){
	if (cursor != cursorBegin)
		cursor--;
	else {
		printf("Error at memory pointer!");
		exit(ERROR);
	}
}

int main(){
	fp = fopen("test1.bf", "r");
	for(int i = 0;i < MEMORY;i++)
		mem[i] = 0;
	cursor = &mem;
	cursorBegin = cursor;

	while(!feof(fp)){
		char c = getCurrentChar();
		switch(c){
		case '+':
			if (*cursor == INT_MAX){
				printf("Error: Max value to cell!\n");
				return 0;
			}
			++*cursor;
			break;
		case '-':
			if (*cursor == INT_MIN){
				printf("Error: Min value to cell!\n");
				return 0;
			}
			--*cursor;
			break;
		case '.':
			putchar(*cursor);
			break;
		case '<':
			moveLeft();
			break;	
		case '>':
			moveRight();
			break;
		case ']':
			if (*cursor == 0) break;
			int counter = 0;
			long pos = ftell(fp);
			while(!feof(fp)){
				if(pos == 0 && c != '['){
					printf("Error: '[' not found!\n");
					return 0;
				}
				fseek(fp,pos-2,0);
				char g = getCurrentChar();
				if(g == '['){
					if (counter == 0)
						break;
					else
						counter--;
				} else if (g == ']'){
					counter++;
				}
				pos = ftell(fp);
				fseek(fp,pos-1,0);
			}
			break;
		default:
			break;
		}
	}
	fclose(fp);
	return 0;
}
